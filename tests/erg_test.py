import datetime
from .context import ergpy
import pytest
from unittest.mock import Mock, patch


def test_convert():
    """convert from string of bytes to integer"""
    assert ergpy.erg._convert(1) == 1
    assert ergpy.erg._convert(1, 2) == 513  # 1 + 2*256
    assert ergpy.erg._convert(1, 2, 3) == 197121  # 1 + 2*256 + 3*256**2

    assert ergpy.erg._convert(1, endian='big') == 1
    assert ergpy.erg._convert(1, 2, endian='big') == 258  # 2 + 1*256
    assert ergpy.erg._convert(1, 2, 3, endian='big') == 66051  # 3 + 2*256 + 256**2


def test_decode_time():
    """Unpack date time from 4 bytes"""
    assert ergpy.erg._decode_time(0x8A, 0x20, 0x30, 0x0C) == datetime.datetime(2016, 10, 8, 12, 48)
    assert ergpy.erg._decode_time(0x0A, 0x21, 0x00, 0x10) == datetime.datetime(2016, 10, 16, 16, 00)
    assert ergpy.erg._decode_time(0x1A, 0x21, 0x00, 0x10) == datetime.datetime(2016, 10, 17, 16, 00)
    assert ergpy.erg._decode_time(0x8A, 0x1E, 0x00, 0x10) == datetime.datetime(2015, 10, 8, 16, 00)
    assert ergpy.erg._decode_time(0x8A, 0x1C, 0x00, 0x10) == datetime.datetime(2014, 10, 8, 16, 00)
    assert ergpy.erg._decode_time(0x0A, 0x1D, 0x00, 0x10) == datetime.datetime(2014, 10, 16, 16, 00)
    assert ergpy.erg._decode_time(0x1A, 0x1D, 0x00, 0x10) == datetime.datetime(2014, 10, 17, 16, 00)
    assert ergpy.erg._decode_time(0x1B, 0x1D, 0x00, 0x10) == datetime.datetime(2014, 11, 17, 16, 00)
    assert ergpy.erg._decode_time(0xFA, 0x1C, 0x00, 0x10) == datetime.datetime(2014, 10, 15, 16, 00)


@patch('ergpy.erg.TIMEOUT', 0.2)
class TestUpdater:
    def test_init(self):
        with pytest.raises(ValueError):
            # unexpected updates
            ergpy.erg.UpdateHandler(key='abc', expected=0)
        with pytest.raises(ValueError):
            # negative updates
            ergpy.erg.UpdateHandler(key='abc', expected=-1)

    def test_single_update(self):
        handler = ergpy.erg.UpdateHandler('abc', expected=1)
        update = {'abc': 1, 'def': 2}
        listen = Mock()
        handler.add_listener(listen)
        with handler:
            handler.update(update)
        listen.assert_called_once_with(update)

    def test_multiple_update(self):
        handler = ergpy.erg.UpdateHandler('abc', expected=2)
        update1 = {'abc': 1, 'def': 2}
        update2 = {'abc': 1, 'ghi': 3}
        listen = Mock()
        handler.add_listener(listen)
        with handler:
            handler.update(update1)
            listen.update.assert_not_called()
            handler.update(update2)
        listen.assert_called_once_with({**update1, **update2})

    def test_broadcast(self):
        handler = ergpy.erg.UpdateHandler('abc')
        listen = Mock()
        handler.add_listener(listen)
        ergpy.erg.UpdateHandler._broadcast(handler, {'abc': 3})
        listen.assert_called_once_with({'abc': 3})

    def test_empty_broadcast(self):
        handler = ergpy.erg.UpdateHandler('abc')
        listen = Mock()
        handler.add_listener(listen)
        ergpy.erg.UpdateHandler._broadcast(handler, {})
        listen.assert_not_called()

    def test_broadcast_continues_past_error(self):
        handler = ergpy.erg.UpdateHandler('abc')
        listen1 = Mock()
        listen2 = Mock()
        handler.add_listener(listen1)
        handler.add_listener(listen2)
        listen1.side_effect = Exception('Update error')

        update = Mock()
        handler._broadcast(update)

        listen1.assert_called_once_with(update)
        listen2.assert_called_once_with(update)

    def test_stopped_handler_ignores_updates(self):
        handler = Mock()
        handler._key = 'abc'
        handler._thread = None
        ergpy.erg.UpdateHandler.update(handler, {'abc': 'def'})
        handler._q.put.assert_not_called()

    def test_update_without_key_property(self):
        handler = ergpy.erg.UpdateHandler('abc')
        with pytest.raises(ValueError):
            handler.update({'def': 4})

    def test_valid_update_is_queued(self):
        handler = Mock()
        handler._key = 'abc'
        ergpy.erg.UpdateHandler.update(handler, {'abc': 1})
        handler._q.put.assert_called_once_with({'abc': 1})

    def test_stop(self):
        handler = Mock()
        wait = Mock()
        ergpy.erg.UpdateHandler.stop(handler, wait)
        assert handler._stop
        assert handler._wait_for_empty == wait

    def test_enter(self):
        handler = Mock()
        ergpy.erg.UpdateHandler.__enter__(handler)
        handler.start.assert_called_once_with()

    def test_run_with_fewer_than_expected_updates(self):
        handler = Mock()
        handler._key = 'abc'
        handler._expected = 3
        handler._current = {}
        handler._get_updates.side_effect = [({'abc': 1, 'def': 2},
                {'abc': 1, 'ghi': 3},
                {'abc': 2, 'def': 4})]
        ergpy.erg.UpdateHandler.run(handler)

        handler._broadcast.assert_called_once_with({'abc': 1, 'def': 2, 'ghi': 3})

    def test_remove_handler(self):
        listen = Mock()
        handler = ergpy.erg.UpdateHandler('key')
        handler.add_listener(listen)

        handler._broadcast({'key': 42})
        listen.assert_called_once_with({'key': 42})
        listen.reset_mock()

        handler.discard_listener(listen)
        handler._broadcast({'key': 27})
        listen.assert_not_called()

@patch('ergpy.erg.BLEAddressType')
@patch('ergpy.erg.GATTToolBackend')
class TestErg:
    def test_configure(self, gatt, addr, *_):
        mac = Mock()
        e = ergpy.erg.Erg(mac, dev='hci0')
        e.configure()
        gatt.assert_called_once_with(hci_device='hci0')
        assert gatt().start.called
        assert e.gatt is gatt()
        gatt().connect.assert_called_once_with(mac, address_type=addr.random, timeout=10)
        assert e.dev is gatt().connect()
        e.dev.char_read.assert_any_call('ce060012-43e5-11e4-916c-0800200c9a66')
        e.dev.char_read.assert_any_call('ce060013-43e5-11e4-916c-0800200c9a66')
        e.dev.char_read.assert_any_call('ce060014-43e5-11e4-916c-0800200c9a66')
        e.dev.char_read.assert_any_call('ce060015-43e5-11e4-916c-0800200c9a66')
        assert e.dev.char_read().decode.call_count == 4
        assert e.configured

    def test_configure_fails_when_connect_fails(self, gatt, addr, *_):
        gatt().connect.side_effect = Exception('start fail')
        e = ergpy.erg.Erg(Mock())
        with pytest.raises(ergpy.erg.ErgException):
            e.configure()
            pytest.fail('Erg was configured when gatt failed')
        assert not e.configured

    def test_erg_not_configured_by_default(self, *_):
        e = ergpy.erg.Erg(Mock())
        assert not e.configured

    def test_starting_unconfigured_erg_fails(self, *_):
        with pytest.raises(ValueError):
            e = Mock()
            e.configured = False
            ergpy.erg.Erg.start(e)
            pytest.fail('Unconfigured erg started without Exception')

    def test_start_with_no_problems(self, *_):
        e = Mock()
        e.configured = True
        ergpy.erg.Erg.start(e)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch31, e._handle31)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch32, e._handle32)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch33, e._handle33)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch35, e._handle35)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch36, e._handle36)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch37, e._handle37)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch38, e._handle38)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch39, e._handle39)
        e.dev.subscribe.assert_any_call(ergpy.erg.ch3a, e._handle3a)

        e._general_handler.start.assert_called_once_with()
        e._stroke_handler.start.assert_called_once_with()
        e._split_handler.start.assert_called_once_with()
        e._summary_handler.start.assert_called_once_with()

    def test_start_fails_if_subscribe_fails(self, *_):
        e = Mock()
        e.configured = True
        e.dev.subscribe.side_effect = Exception('Subscribe Error')
        ergpy.erg.Erg.start(e)
        e.dev.subscribe.assert_called_once_with(ergpy.erg.ch31, e._handle31)
        e._general_handler.assert_not_called()
        e.stop.assert_called_once_with()

    def test_stop_expected_behaviour(self, *_):
        e = Mock()
        ergpy.erg.Erg.stop(e)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch31)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch32)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch33)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch35)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch36)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch37)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch38)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch39)
        e.dev.unsubscribe.assert_any_call(ergpy.erg.ch3a)

        e._general_handler.stop.assert_called_once_with(True)
        e._stroke_handler.stop.assert_called_once_with(True)
        e._split_handler.stop.assert_called_once_with(True)
        e._summary_handler.stop.assert_called_once_with(True)

    def test_handlers_are_stopped_if_unsubscribe_fails(self, *_):
        e = Mock()
        e.dev.unsubscribe.side_effect = Exception('unsub failed')
        ergpy.erg.Erg.stop(e)
        e._general_handler.stop.assert_called_once_with(True)

    def test_disconnect(self, *_):
        e = Mock()
        ergpy.erg.Erg.disconnect(e)
        e.dev.disconnect.assert_called_once_with()
        e.gatt.stop.assert_called_once_with()

    def test_disconnect_when_disconnect_fails(self, *_):
        e = Mock()
        e.dev.disconnect.side_effect = Exception('Disconnect failed')
        ergpy.erg.Erg.disconnect(e)
        e.gatt.stop.assert_not_called()

    def test_enter(self, *_):
        e = Mock()
        ergpy.erg.Erg.__enter__(e)
        e.configure.assert_called_once_with()
        e.start.assert_called_once_with()

    def test_enter_with_configure_error(self, *_):
        e = Mock()
        e.configure.side_effect = Exception('Configure Error')
        with pytest.raises(Exception):
            ergpy.erg.Erg.__enter__(e)
            pytest.fail('Enter buried exception')
        e.start.assert_not_called()

    def test_exit(self, *_):
        e = Mock()
        ergpy.erg.Erg.__exit__(e, Mock(), Mock(), Mock())
        e.stop.assert_called_once_with()
        e.disconnect.assert_called_once_with()

    def test_exit_with_stop_errors(self, *_):
        e = Mock()
        e.stop.side_effect = Exception('Stop failed')
        ergpy.erg.Erg.__exit__(e, Mock(), Mock(), Mock())

    def test_exit_with_disconnect_errors(self, *_):
        e = Mock()
        e.disconnect.side_effect = Exception('disconnect failed')
        ergpy.erg.Erg.__exit__(e, Mock(), Mock(), Mock())


@patch('ergpy.erg.WorkoutDurationType')
@patch('ergpy.erg.StrokeState')
@patch('ergpy.erg.RowingState')
@patch('ergpy.erg.WorkoutState')
@patch('ergpy.erg.IntervalType')
@patch('ergpy.erg.WorkoutType')
class TestErgHandlerMethods:  # split to have seperate patching
    def test_handle_3a(self, work_type, int_type, *_):
        a = Mock()
        ergpy.erg.Erg._handle3a(
            a,
            0x3a,
            bytearray([
                0x7c, 0x20,  # log entry date
                0x1a, 0x16,  # log entry time
                0x0,  # split type
                0x1e, 0x0,  # split size
                0x1,  # split count
                0x4, 0x0,  # total calories
                0x51, 0x0,  # power
                0x0, 0x0, 0x0,  # total rest distance
                0x0, 0x0,  # interval rest time
                0x48, 0x2  # average calories
            ])
        )
        int_type.assert_called_once_with(0)
        a._summary_handler.update.assert_called_once_with({
            'log_entry_datetime': datetime.datetime(2016, 12, 7, 22, 26),
            'interval_type': int_type(),
            'interval_size': 30,
            'interval_count': 1,
            'total_calories': 4,
            'average_power': 81,
            'total_rest_distance': 0,
            'interval_rest_time': 0,
            'average_calories': 584
        })

    def test_handle3a_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle3a(a, 0x3a, [0]*19)
            pytest.fail('_handle3a didn\'t throw exception')

    def test_handle3a_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 3a)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle3a(a, 0x3a, bytearray([0]*21))
            pytest.fail('_handle3a didn\'t throw exception')

    def test_handle_39(self, work_type, *_):
        a = Mock()
        ergpy.erg.Erg._handle39(
            a,
            39,
            bytearray([
                0x7c, 0x20,  # log entry date
                0x1a, 0x16,  # log entry time
                0xb8, 0xb, 0x0,  # elapsed
                0x98, 0x3, 0x0,  # distance
                0x18,  # average rate
                0x0,  # ending hr
                0x0,  # average hr
                0x0,  # min hr
                0x0,  # max hr
                0x7a,  # average drag
                0x0,  # recovery hr
                0x5,  # workout type
                0x5e, 0x6  # average pace
            ])
        )
        work_type.assert_called_once_with(5)
        a._summary_handler.update.assert_called_once_with({
            'log_entry_datetime': datetime.datetime(2016, 12, 7, 22, 26),
            'elapsed': 30.0,
            'distance': 92.0,
            'average_rate': 24,
            'ending_heartrate': 0,
            'average_heartrate': 0,
            'min_heartrate': 0,
            'max_heartrate': 0,
            'average_drag_factor': 122,
            'recovery_heartrate': 0,
            'workout_type': work_type(),
            'average_pace': 163.0
        })

    def test_handle39_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle39(a, 39, [0]*19)
            pytest.fail('_handle39 didn\'t throw exception')

    def test_handle39_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 39)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle39(a, 39, bytearray([0]*21))
            pytest.fail('_handle39 didn\'t throw exception')

    def test_handle_38(self, *_):
        a = Mock()
        ergpy.erg.Erg._handle38(
            a,
            38,
            bytearray([
                0xb8, 0xb, 0x0,  # elapsed
                0x18,  # average rate
                0x0,  # work hr
                0x0,  # rest hr
                0x4e, 0x6,  # average pace
                0x4, 0x0,  # total calories
                0x4b, 0x2,  # average calories
                0x1c, 0xc,  # interval speed
                0x53, 0x0,  # interval power
                0x7a,  # average drag
                0x1  # interval number
            ])
        )
        a._split_handler.update.assert_called_once_with({
            'elapsed': 30.0,
            'average_rate': 24,
            'work_heartrate': 0,
            'rest_heartrate': 0,
            'average_pace': 161.4,
            'average_calories': 587,
            'total_calories': 4,
            'speed': 3.1,
            'power': 83,
            'average_drag_factor': 122,
            'number': 1
        })

    def test_handle38_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle38(a, 38, [0]*17)
            pytest.fail('_handle38 didn\'t throw exception')

    def test_handle38_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 38)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle38(a, 38, bytearray([0]*19))
            pytest.fail('_handle38 didn\'t throw exception')

    def test_handle_37(self, work_type, int_type, *_):
        a = Mock()
        ergpy.erg.Erg._handle37(
            a,
            37,
            bytearray([
                0xb8, 0xb, 0x0,  # elapsed
                0x9f, 0x3, 0x0,  # distance
                0x2c, 0x1, 0x0,  # split time
                0x5d, 0x0, 0x0,  # split distance
                0x0, 0x0,  # rest time
                0x0, 0x0,  # rest distance
                0x0,  # split type
                0x1  # split number
            ])
        )
        int_type.assert_called_once_with(0)
        a._split_handler.update.assert_called_once_with({
            'elapsed': 30.0,
            'distance': 92.7,
            'interval_time': 30.0,
            'interval_distance': 93,
            'rest_time': 0,
            'rest_distance': 0,
            'type': int_type(),
            'number': 1
        })

    def test_handle37_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle37(a, 37, [0]*17)
            pytest.fail('_handle37 didn\'t throw exception')

    def test_handle37_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 37)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle37(a, 37, bytearray([0]*19))
            pytest.fail('_handle37 didn\'t throw exception')

    def test_handle_36(self, *_):
        a = Mock()
        ergpy.erg.Erg._handle36(
            a,
            36,
            bytearray([
                0x7d, 0xa, 0x0,  # elapsed
                0x5e, 0x0,  # stroke power
                0x6f, 0x2,  # stroke calories
                0xb, 0x0,  # stroke count
                0x0, 0x0, 0x0,  # projected work time
                0x5c, 0x0, 0x0  # projected work distance
            ])
        )
        a._stroke_handler.update.assert_called_once_with({
            'elapsed': 26.85,
            'power': 94,
            'calories': 623,
            'stroke_count': 11,
            'projected_time': 0,
            'projected_distance': 92
        })

    def test_handle36_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle36(a, 36, [0]*14)
            pytest.fail('_handle36 didn\'t throw exception')

    def test_handle36_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 36)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle36(a, 36, bytearray([0]*16))
            pytest.fail('_handle36 didn\'t throw exception')

    def test_handle_35(self, *_):
        a = Mock()
        ergpy.erg.Erg._handle35(
            a,
            35,
            bytearray([
                0x29, 0x8, 0x0,  # elapsed
                0x78, 0x2, 0x0,  # distance
                0x6a,  # drive length
                0x44,  # drive time
                0xcc, 0x0,  # recovery time
                0xb5, 0x3,  # stroke distance
                0x6, 0x4,  # peak force
                0xb4, 0x2,  # average force
                0xe2, 0xa,  # work per stroke
                0x9, 0x0  # stroke count
            ])
        )
        a._stroke_handler.update.assert_called_once_with({
            'elapsed': 20.89,
            'distance': 63.2,
            'drive_length': 1.06,
            'drive_time': 0.68,
            'recovery_time': 2.04,
            'stroke_distance': 9.49,
            'peak_force': 103.0,
            'average_force': 69.2,
            'work_per_stroke': 278.6,
            'stroke_count': 9
        })

    def test_handle35_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle35(a, 35, [0]*19)
            pytest.fail('_handle35 didn\'t throw exception')

    def test_handle35_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 35)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle35(a, 35, bytearray([0]*21))
            pytest.fail('_handle35 didn\'t throw exception')

    def test_handle_33(self, *_):
        a = Mock()
        ergpy.erg.Erg._handle33(
            a,
            33,
            bytearray([
                0x68, 0x9B, 0x01,  # elapsed time
                0x04,  # interval number
                0xEA, 0x00,  # avg power
                0xF6, 0x02,  # total calories
                0x8F, 0x2F,  # split avg pace
                0xE2, 0x00,  # split avg power
                0xE2, 0x02,  # split avg calories
                0x73, 0x08, 0x00,  # last split time
                0xF2, 0x03, 0x00,  # last split dist
            ])
        )
        a._general_handler.update.assert_called_once_with({
            'elapsed': 1053.2,
            'interval_count': 4,
            'average_power': 234,
            'total_calories': 758,
            'split_average_pace': 121.75,
            'split_average_power': 226,
            'split_average_calories': 738,
            'last_split_time': 216.3,
            'last_split_distance': 1010,
        })

    def test_handle33_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle33(a, 33, [0]*19)
            pytest.fail('_handle33 didn\'t throw exception')

    def test_handle33_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 33)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle33(a, 33, bytearray([0]*21))
            pytest.fail('_handle33 didn\'t throw exception')

    def test_handle_32(self, *_):
        a = Mock()
        ergpy.erg.Erg._handle32(
            a,
            32,
            bytearray([
                0x68, 0x9B, 0x01,  # elapsed time
                0x2A, 0x37,  # speed
                18,  # rate
                0x9C,  # hr
                0xC9, 0x31,  # pace
                0x65, 0x31,  # avg pace
                0xD2, 0x04,  # rest dist
                0x0C, 0x91, 0x00])  # rest time
        )
        a._general_handler.update.assert_called_once_with({
            'elapsed': 1053.2,
            'speed': 14.122,
            'rate': 18,
            'heartrate': 156,
            'pace': 127.45,
            'average_pace': 126.45,
            'rest_distance': 1234,
            'rest_time': 371.32,
        })

    def test_handle32_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle32(a, 32, [0]*15)
            pytest.fail('_handle32 didn\'t throw exception')

    def test_handle32_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 32)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle32(a, 32, bytearray([0]*17))
            pytest.fail('_handle32 didn\'t throw exception')

    def test_handle_31(self, work_type, int_type, work_state,  row_state, stroke_state, work_dur_type, *_):
        a = Mock()
        ergpy.erg.Erg._handle31(
                a,
                31,
                bytearray([1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 1, 12, 13, 14, 15, 16, 17, 0, 19])
        )
        work_type.assert_called_once_with(7)
        int_type.assert_called_once_with(8)
        work_state.assert_called_once_with(9)
        row_state.assert_called_once_with(1)
        stroke_state.assert_called_once_with(1)
        work_dur_type.assert_called_once_with(0)
        a._general_handler.update.assert_called_once_with({
            'elapsed': 1971.21,
            'distance': 39450.0,
            'workout_type': work_type(),
            'interval_type': int_type(),
            'workout_state': work_state(),
            'rowing_state': row_state(),
            'stroke_state': stroke_state(),
            'total_work_distance': 920844,
            'workout_duration': 11182.23,
            'workout_duration_type': work_dur_type(),
            'drag_factor': 19
        })

    def test_handle31_too_few_bytes(self, *_):
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle31(
                    a,
                    31,
                    bytearray([0]*18)
            )
            pytest.fail('_handle31 didn\'t throw exception')

    def test_handle31_too_many_bytes(self, *_):
        """Check that extra bytes are not just ignored (for 31)"""
        with pytest.raises(ValueError):
            a = Mock()
            ergpy.erg.Erg._handle31(a, 31, bytearray([0]*21))
            pytest.fail('_handle31 didn\'t throw exception')


def test_dat_length_check():
    with pytest.raises(ValueError):
        ergpy.erg._check_dat_length(range(10), 9)
    with pytest.raises(ValueError):
        ergpy.erg._check_dat_length(range(10), 9)
    ergpy.erg._check_dat_length(range(10), 10)


@patch('ergpy.erg.GATTToolBackend')
def test_discover(gatttool):
    dev, t =  Mock(), Mock()
    devs = ergpy.erg.discover(dev, timeout=t)
    gatttool.assert_called_once_with(hci_device=dev)
    gatttool().reset.assert_called_once_with()
    gatttool().scan.assert_called_once_with(timeout=t)
    assert devs == gatttool().scan()


@patch('ergpy.erg.GATTToolBackend')
def test_discover_with_gatt_error(gatttool):
    gatttool().scan.side_effect = Exception('scan error')
    with pytest.raises(ergpy.erg.ErgException):
        ergpy.erg.discover()

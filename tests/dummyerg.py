from io import StringIO
import threading
import time
from ast import literal_eval
from .context import ergpy
import logging

log = logging.getLogger('ergpy.dummy_erg')


class Erg(ergpy.erg.Erg):
    def __init__(self, dummy_file='tests/testdata/intervals', *args):
        super().__init__('dummy')
        self._data_file = dummy_file
        self.channels = {29: self._handle31,
                32: self._handle32,
                35: self._handle33,
                40: self._handle35,
                43: self._handle36,
                46: self._handle37,
                49: self._handle38,
                52: self._handle39,
                55: self._handle3a
                }
    def configure(self, mode='single'):
        log.info('configuring')
        self.interrupt = False
        with open(self._data_file) as dat:
            self.data = StringIO(dat.read())

    def start(self):
        log.info('starting')
        self._general_handler.start()
        self._stroke_handler.start()
        self._split_handler.start()
        self._summary_handler.start()

        self._thread = threading.Thread(target=self._run, name='dummy_erg')
        self._thread.start()

    def _run(self):
        log.info('run')
        start = time.time()
        log.info(f'start time: {start}')
        for line in self.data:
            ts, ch, dat = (literal_eval(part) for part in line.split(';'))
            if self.interrupt:
                return

            while time.time() < start + ts:
                time.sleep(0.01)
            self.channels[ch](ch, bytearray(dat))

    def stop(self):
        log.info('stop')
        self.interrupt = True
        self._general_handler.stop(True)
        self._stroke_handler.stop(True)
        self._split_handler.stop(True)
        self._summary_handler.stop(True)


    def disconnect(self):
        log.info('disconnect')
        pass

def discover(*a, **kw):
    log.info('dummy discover')
    return [{'name': 'pseudo', 'address': '01:23:45:67:89:AB'}]

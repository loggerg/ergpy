import pytest
import tempfile

from imp import reload
# from unittest.mock import Mock, patch

@pytest.fixture
def config(scope='function'):
    from .context import ergpy
    yield(ergpy.config)
    reload(ergpy)

@pytest.fixture
def conf_file(scope='function'):
    conf = tempfile.NamedTemporaryFile(mode='w+t')
    conf.write('abc = "def"')
    conf.flush()
    yield conf.name
    conf.close()

class TestConfiguration:
    @pytest.mark.parametrize('key', ('abcd', '_efgh'))
    def test_unknown_key(self, key, config):
        with pytest.raises(AttributeError):
            getattr(config, key)

    def test_get_value(self, config):
        assert config.format == 'tcx'

    def test_set_value(self, config):
        assert config.format == 'tcx'
        config.format = 'fit'
        assert config.format == 'fit'

    def test_contains(self, config):
        assert 'abcdef' not in config
        config.abcdef = 42
        assert 'abcdef' in config

    def test__doesnt_contain_local_keys(self, config):
        assert '_abcd' not in config
        config._abcd = 42
        assert '_abcd' not in config

    def test_del_keys(self, config):
        assert config.format == 'tcx'
        del config.format
        with pytest.raises(AttributeError):
            config.format

    def test_del_local(self, config):
        config._abc = 42
        assert config._abc == 42
        del config._abc
        with pytest.raises(AttributeError):
            config._abc

    def test_init(self, config):
        assert config.format == 'tcx'
        config.format = 'fit'
        assert config.format == 'fit'
        config.init()
        assert config.format == 'tcx'

    def test_init_from_file(self, config, conf_file):
        with pytest.raises(AttributeError):
            config.abc
        print(conf_file)
        config.init(conf_file)
        print(config._conf)
        assert config.abc == 'def'

    def test_init_from_missing_file(self, config, tmpdir):
        missing_config = tmpdir.join('missing_conf')
        assert not missing_config.exists()
        with pytest.raises(ValueError):
            config.init(str(missing_config))

    def test_store_config(self, config):
        try:
            conf = tempfile.NamedTemporaryFile(mode='w+t')
            config.init(conf.name)
            config.store()
            assert 'format = "tcx"' in conf.read()
        finally:
            conf.close()

    def test_store_new_value(self, config, conf_file):
        assert 'new_var' not in config
        config.init(conf_file)
        config.new_var = 42
        assert config.new_var == 42
        config.store()
        assert config.new_var == 42
        with open(conf_file) as updated:
            conf = updated.readlines()
            assert 'new_var = 42\n' in conf



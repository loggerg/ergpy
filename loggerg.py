#!/usr/bin/env python
# -*- coding: utf-8 -*-

from ergpy import erg
from ergpy import ErgException
from ergpy import config
from ergpy import recorder
from ergpy.db import ergdb
from ergpy.export import tcx
import os
import click
import logging
from datetime import datetime, timedelta

log = logging.getLogger('loggerg')

NO_ERG_ERROR = """No rowing machines in range
Please check wireless is enabled on your PM5"""

def clog(msg, *a, **kw):
    click.echo(msg.format(*a, **kw))

def _log_level(level):
    return min(60, max(0, level))

def _validate_mac(ctx, param, mac):
    if ctx.obj['testing']:
        return mac
    if mac is None:
        return
    parts = mac.split(':')
    try:
        if len(parts) != 6 or not all(0 <= int(p,16) < 256 for p in parts):
            raise ValueError()
    except ValueError as ve:
        raise click.BadParameter('erg mac needs to be in 01:23:45:67:89:AB format')
    return mac

def _validate_erg(ctx, param, name):
    if name is None:
        return
    if name in config.ergs:
        mac = config.ergs[name]['mac']
        _validate_mac(ctx, param, mac)
        return mac
    return _validate_mac(ctx, param, name)

def _default_tcx_file(s):
    date = s.log_entry_datetime
    distance = s.distance
    time = s.elapsed
    fname = config.file_template.format(
        date=date,
        time=time,
        distance=distance)

    return os.path.join(config.output_dir, fname)

def _write_tcx(session_id, fh, pretty_print=True, compress=None):
    fh = os.path.expanduser(fh)
    click.echo('Writing to {}'.format(fh))
    tcx.TcxSession(session_id).write(
            fh,
            pretty_print=pretty_print,
            compression=compress,
            encoding='utf-8',
            xml_declaration=True
    )
    return fh

def _strava_upload(tcx):
    from ergpy.export import strava
    strava.authorize()
    strava.upload(tcx)


@click.command(name='list', short_help='Show previous sessions')
@click.option('-l','--limit', 'limit', default=0, help='limit to most recent number of sessions')
def list_history(limit):
    """Show brief description of each session in history"""
    with ergdb.db_session:
        q = ergdb.Session.select().order_by(lambda s: ergdb.desc(s.date))
        if limit:
            q = q.limit(limit)
        for s in q:
            print(s)

@click.command(name='summary', short_help='Show summary of sessions')
def summary_history():
    """Show short summary of all sessions in history"""
    with ergdb.db_session:
        filt = lambda s: s.elapsed and s.distance
        total_seconds = sum(s.elapsed for s in ergdb.Session.select(filt))
        total_time = timedelta(seconds=total_seconds)
        total_distance = sum(s.distance for s in ergdb.Session.select(filt))
        click.echo('Total time: {time}\nTotal distance: {distance}m'.format(time=total_time, distance=total_distance))

@click.group(name='history')
def history():
    """Show details of previous sessions"""
    pass

history.add_command(list_history)
history.add_command(summary_history)

@click.group()
def export():
    pass

@export.command('tcx')
@click.argument('activity')
@click.argument('target')
def export_tcx(activity, target):
    with ergdb.db_session:
        tcx.TcxSession(activity).write(target, pretty_print=' ', xml_declaration=True, encoding='utf-8')

@export.command('strava')
@click.argument('activity', nargs=-1)
def export_strava(activity):
    from ergpy.export import strava
    import tempfile
    with ergdb.db_session:
        strava.authorize()
        for act in activity:
            with tempfile.NamedTemporaryFile(mode='w+b') as target:
                exp = _write_tcx(act, target.name)
                _strava_upload(exp)


@click.option('-t', '--timeout', 'timeout', default=5)
@click.option('-d', '--dev', 'dev', default='hci0')
@click.command()
@click.pass_context
def discover(ctx, timeout, dev):
    """Find Concept2 PM5s in range"""
    try:
        ergs = erg.discover(dev=dev, timeout=timeout)
    except ErgException as e:
        click.echo(e.message, err=True)
    else:
        if not ergs:
            click.echo(NO_ERG_ERROR, err=True)
        else:
            for e in ergs:
                click.echo("{name}: {add}".format(name=e['name'], add=e['address']))

@click.group(name='erg')
def command_erg():
    """Configure ergs"""
    pass

@click.command(name='list', short_help='List ergs in the configuration')
def list_ergs():
    """List ergs currently configured"""
    ergs = config.ergs
    if not ergs:
        clog('No ergs configured')
    for e, v in config.ergs.items():
        print('{e}: {mac}'.format(e=e, mac=v['mac']))

@click.command(name='add', short_help='Add an erg to the configuration')
@click.argument('name')
@click.argument('mac', callback=_validate_mac)
def add_erg(name, mac):
    """
    Add a new erg to the configuration

    \b
    NAME local name to refer to the erg by
    MAC  the mac address of the erg
    """
    if name in config.ergs:
        raise click.BadArgumentUsage('Name already exists, use edit to update')
    config.ergs[name] = {'mac': mac}
    config.store()

command_erg.add_command(list_ergs)
command_erg.add_command(add_erg)

@click.option('-d', '--dev', 'dev', default='hci0', help='Bluetooth device to use (/dev/hci0 would be hci0)')
@click.option('-s', '--strava', 'strava', is_flag=True, help='Automatically upload session to strava')
@click.option('-f', '--file', 'write', default=False, is_flag=True,  help='Save session to file')
@click.option('-o', '--out', 'outfile', default=None, type=click.Path(), help='File to save session to if not default')
@click.option('-e', '--erg', 'erg_mac', callback=_validate_erg,
        help='The erg to use (either mac or name from config)', required=True)
@click.command()
@click.pass_context
def record(ctx, outfile, write, strava, erg_mac, dev):
    """Record rowing session

    by default sessions are recorded to database, if file is required,
    specify filename"""
    # if config.ergs.get(erg_mac, None):
    clog('Using erg: {}', erg_mac)

    rec = recorder.Recorder()
    e = ctx.obj['erg'](erg_mac)
    with e:
        session_id = rec.run(e)
    with ergdb.db_session:
        if write or outfile or strava:
            s = ergdb.Session[session_id]
            outfile = outfile or _default_tcx_file(s)
            try:
                click.echo('Writing session to {}'.format(outfile))
                tcx = _write_tcx(session_id, outfile)
                if strava:
                    _strava_upload(tcx)
            except Exception as e:
                click.echo('Failed to save tcx file', err=True)
                log.error('Export failed', exc_info=e)
                print(e)
        session = ergdb.Session[session_id]
        click.echo(format(session, 'erg'))

@click.group('config')
def command_config():
    pass

@command_config.command('edit')
def edit_config():
    """Open config in $EDITOR for editing"""
    click.edit(filename=config._rc_file)

@command_config.command('show')
@click.argument('key')
def show_config(key):
    """Show the value of key in config"""
    try:
        click.echo('{}: {}'.format(key, getattr(config, key)))
    except AttributeError as ke:
        click.echo('No such key in config')

@command_config.command('delete')
@click.argument('key')
def show_config(key):
    """Show value of key in config"""
    try:
        delattr(config, key)
    except KeyError as ke:
        click.echo('No such key in config')
    config.store()

@command_config.command('add')
@click.argument('key')
@click.argument('value')
def add_config(key, value):
    """Set value of key in config"""
    setattr(config, key, value)
    config.store()

@click.option('-q', '--quiet', count=True, help='Reduce output to console (additive (-qq is quieter))')
@click.option('-v', '--verbose', count=True, help='Increase output to console')
@click.option('-c', '--config', 'rcfile',
        type=click.Path(exists=True, dir_okay=False, resolve_path=True),
        envvar='LOGGERGRC', help='File to use as config - defaults to $LOGGERGRC or ~/.loggergrc')
@click.option('-x', '--testing', 'test', is_flag=True, help='Run in test mode')
@click.group()
@click.pass_context
def cli(ctx, quiet, verbose, rcfile, test):
    """Command line tool for recording rowing sessions from PM5"""
    if rcfile:
        clog('Using config {}', rcfile)
        config.init(rcfile)
    if test:
        clog('Using test mode')
        global erg
        import tests.dummyerg as erg
        config.database = config.database[:-3] + '_test.db'
    ergdb.init(config.database)
    ctx.obj['erg'] = erg.Erg
    ctx.obj['testing'] = test

cli.add_command(discover)
cli.add_command(record)
cli.add_command(command_erg)
cli.add_command(history)
cli.add_command(export)
cli.add_command(command_config)

if __name__ == "__main__":
    logging.basicConfig(filename='log', level=logging.WARN, format='{asctime:<15s} {levelname:>8s} - {threadName} - {name:s}.{funcName} - {message:s}', style='{')
    logging.getLogger('ergpy').setLevel(logging.DEBUG)
    rcfile = os.path.join(click.get_app_dir('loggerg'), 'config')
    if os.path.exists(rcfile):
        config.init(rcfile)
    cli(obj={})

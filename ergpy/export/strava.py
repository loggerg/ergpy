import os
import time
from urllib import parse
import threading
import webbrowser
import blindspin
from http.server import BaseHTTPRequestHandler, HTTPServer
from ergpy import config, HOME
from stravalib import Client

client_id = os.environ.get('LOGGERG_CLIENT_ID', None)
client_key = os.environ.get('LOGGERG_CLIENT_KEY', None)

# Should never be used but give some feedback of success if redirect fails
THANK_YOU_TITLE = 'Success'
THANK_YOU_MSG = 'Thank you, you can now close your browser.'

SUCCESS_PAGE = '''
<html>
<head>
<title>{title}</title>
</head>
<body><h3>{msg}</h3></body>
</html>'''.format(title=THANK_YOU_TITLE, msg=THANK_YOU_MSG).encode('utf-8')


class _Auth(BaseHTTPRequestHandler):
    def do_GET(self):
        """Get `code` out of url encoded query"""
        url = parse.urlparse(self.path)
        query = parse.parse_qs(url.query)
        if 'code' in query:
            config.auth = query['code'][0]
        self.send_response(303)
        self.send_header('Content-type', 'text/html')
        self.send_header('Location', HOME + '/authorised')
        self.end_headers()
        self.wfile.write(SUCCESS_PAGE)
        self.close_connection = True

    def log_message(self, fmt, *args):
        """Ignore connection messages"""
        pass


def authorize():
    if 'auth' in config:
        return
    if client_id is None or client_key is None:
        raise ValueError('LOGGERG_CLIENT_KEY and LOGGERG_CLIENT_ID environment variables are required')

    httpd = HTTPServer(('localhost', 5000), _Auth)
    httpd.timeout = 300  # Wait 5 minutes for user to authenticate
    t = threading.Thread(target=httpd.handle_request)
    t.start()
    client = Client()
    url = client.authorization_url(
            client_id=client_id,
            redirect_uri='http://localhost:5000/authorization',
            scope='write,view_private')
    webbrowser.open(url)
    # a pain but stops the "open in current window" message writing over the
    # spinner
    time.sleep(1)
    with blindspin.Spinner():
        print('\r Waiting for authorization  ', end='')
        t.join()
        print()
    if 'auth' not in config:
        raise ValueError('Authenication failed')
    token = client.exchange_code_for_token(
            client_id=client_id,
            client_secret=client_key,
            code=config.auth)
    config.auth = token
    config.store()


def upload(fname):
    if 'auth' not in config:
        raise ValueError('User is not authorised to upload to strava')
    client = Client(access_token=config.auth)
    with open(fname) as tcx:
        with blindspin.Spinner():
            upl = client.upload_activity(tcx, 'tcx', activity_type='rowing', private=config.private)
            upl.wait(config.strava_timeout)

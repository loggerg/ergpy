from ergpy.db import ergdb
from ergpy import config
from itertools import groupby
from lxml import etree as ET
from math import floor
from datetime import datetime, timedelta

ROOT = 'TrainingCenterDatabase'
ACTS = 'Activities'
ACT = 'Activity'
ID = 'Id'
TRACK = 'Track'
LAP = 'Lap'
POINT = 'Trackpoint'

ns = 'http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2'
ns2 = 'http://www.garmin.com/xmlschemas/UserProfile/v2'
ns3 = 'http://www.garmin.com/xmlschemas/ActivityExtension/v2'
ns4 = 'http://www.garmin.com/xmlschemas/ProfileExtension/v1'
ns5 = 'http://www.garmin.com/xmlschemas/ActivityGoals/v1'
xsi = 'http://www.w3.org/2001/XMLSchema-instance'
xsi_schema = ('http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2' ' '
    'http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd')

nsmap = {None: ns, 'ns2': ns2, 'ns3': ns3, 'ns4': ns4, 'ns5': ns5, 'xsi': xsi}

TF = '%Y-%m-%dT%H:%M:%S.%fZ'

class TcxSession:
    def __init__(self, session_id):
        self.session_id = session_id
        self.root = ET.Element(ROOT,
                attrib={'{{{}}}schemaLocation'.format(xsi): xsi_schema},
                nsmap=nsmap)
        self.session = None
        self._built = False
        self._distance = 0
        self._time = 0

    def write(self, fh,  **kw):
        self._build()
        tree = ET.ElementTree(self.root)
        tree.write(fh, **kw)

    @ergdb.db_session
    def _build(self):
        self.session = ergdb.Session[self.session_id]
        act = self.add_boilerplate()
        self.add_data(act)
        self.add_creator()
        self._built = True

    def add_boilerplate(self):
        acts = ET.SubElement(self.root, ACTS)
        act = ET.SubElement(acts, ACT, Sport='Rowing')
        ET.SubElement(act, ID).text = self.session.date.strftime(TF)
        return act

    def add_data(self, act):
        self._last_written = (0, 0) # dist, time
        for (lap, strokes), (_, updates) in zip(self.session.split_strokes(), self.session.split_updates()):
            print('lap: {}'.format(lap))
            self.add_lap(act, _zip_by_time(strokes, updates), lap)

    def add_lap(self, act, data, lap_info):
        start_time = (self.session.date+timedelta(seconds=floor(self._time))).strftime(TF)
        lap = ET.SubElement(act, LAP, StartTime=start_time)
        track = ET.SubElement(lap, TRACK)
        point = ET.SubElement(track, POINT)
        ET.SubElement(point, 'Time').text = start_time
        ET.SubElement(point, 'DistanceMeters').text = '{:.2f}'.format(self._distance)
        for stroke, update in data:
            self._create_trackpoint(track, stroke, update)
        point = ET.SubElement(track, POINT)
        self._distance += lap_info['distance']
        self._time += lap_info['time']
        ET.SubElement(point, 'Time').text = (self.session.date + timedelta(seconds=self._time)).strftime(TF)
        ET.SubElement(point, 'DistanceMeters').text = '{:.2f}'.format(self._distance)

    def add_creator(self):
        pass

    def _create_trackpoint(self, track, stroke, update):
        if update or stroke:
            point = ET.SubElement(track, POINT)
            if update:
                ET.SubElement(point, 'Time').text = (self.session.date + timedelta(seconds=floor(self._time+update.elapsed))).strftime(TF)
                dist = _interp(self._last_written, (update.distance, update.elapsed), floor(update.elapsed))
                ET.SubElement(point, 'DistanceMeters').text = '{:.2f}'.format(self._distance + dist)
                self._last_written = update.distance, update.elapsed
                if update.rate:
                    ET.SubElement(point, 'Cadence').text = '{}'.format(update.rate)
                if update.heartrate:
                    hr = ET.SubElement(point, 'HeartRateBpm')
                    ET.SubElement(hr, 'Value').text = '{}'.format(update.heartrate)
            if (stroke and stroke.power) or (update and update.speed):
                ext = ET.SubElement(point, 'Extensions')
                tpx = ET.SubElement(ext, '{{{}}}TPX'.format(ns3))
                if stroke and stroke.power:
                    ET.SubElement(tpx, '{{{}}}Watts'.format(ns3)).text = '{}'.format(stroke.power)
                if update and update.speed:
                    ET.SubElement(tpx, '{{{}}}Speed'.format(ns3)).text = '{:.2f}'.format(update.speed)

def _zip_by_time(strokes, updates):
    time = 1
    update_iter = iter(updates)
    stroke_iter = iter(strokes)
    current_update = next(update_iter)
    current_stroke = next(stroke_iter)
    u, s = None, None
    while True:
        while current_update.elapsed < time:
            u = current_update
            current_update = next(update_iter)
        while current_stroke.elapsed < time:
            s = current_stroke
            current_stroke = next(stroke_iter)
        yield s, u
        time += 1


def _interp(pre, post, target):
    if target == pre[1]: return pre[0]
    if pre[1] == post[1]: return pre[0]
    res = pre[0] + (target - pre[1])/(post[1] - pre[1])*(post[0] - pre[0])
    return res

VERSION = '0.0.1'
NAME = 'loggerg'
HOME = 'http://loggerg.gitlab.io/'

class ErgException(Exception):
    def __init__(self, msg=None, cause=None):
        super().__init__(msg, cause)
        self.message = msg

from . import erg
# Initialise config
from .config import Configuration
config = Configuration()
del Configuration

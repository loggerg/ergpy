#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import datetime
import queue
from queue import Queue
from . import erg
from .db import ergdb
log = logging.getLogger('ergpy.recorder')


class Recorder:
    def __init__(self, single=True):
        self._log = log.getChild('Recorder')
        self._state = erg.RowingState.INACTIVE
        self.id = None
        self.session = None
        self._listen = False
        self._complete = False
        self._interrupted = False
        self._queue = Queue()

    def queue_update(self, upd):
        if self._listen:
            self._queue.put((self.update, upd))

    def queue_stroke(self, stroke):
        if self._listen:
            self._queue.put((self.new_stroke, stroke))

    def queue_split(self, spl):
        if self._listen:
            self._queue.put((self.new_split, spl))

    def queue_summary(self, summ):
        if self._listen:
            self._queue.put((self.new_summary, summ))

    def _updates(self):
        while not self._complete:
            try:
                yield self._queue.get(5)
            except queue.Empty as _:
                self._complete = True

    def run(self, erg):
        erg.add_stroke_handler(self.queue_stroke)
        erg.add_general_handler(self.queue_update)
        erg.add_split_handler(self.queue_split)
        erg.add_summary_handler(self.queue_summary)

        try:
            with ergdb.db_session:
                self._listen = True
                for f, upd in self._updates():
                    f(upd)
        except KeyboardInterrupt as ke:
            self._log.info('KeyboardInterrupt in erg recorder')
            self._interrupted = True
            self._complete = True
        finally:
            self._listen = False
            erg.remove_stroke_handler(self.queue_stroke)
            erg.remove_split_handler(self.queue_split)
            erg.remove_summary_handler(self.queue_summary)
            erg.remove_general_handler(self.queue_update)

        return self.id

    def update(self, data):
        state = data.get('rowing_state', erg.RowingState.INACTIVE)
        self._log.debug('update received: state: %s', state)
        if state == erg.RowingState.INACTIVE:
            if self.active:
                self._log.debug('Active to inactive')
            wstate = data.get('workout_state', erg.WorkoutState.WORKOUTEND)
            if wstate == erg.WorkoutState.WORKOUTLOGGED:
                self._complete = True
            self._state = erg.RowingState.INACTIVE
            return
        elif not self.active and state == erg.RowingState.ACTIVE:
            self._log.info('INACTIVE to ACTIVE')
            if not self.session:
                self.session = ergdb.Session(date=datetime.datetime.now())
                ergdb.commit()
                self.id = self.session.id
                self._state = state
                self._log.info('session: %s', self.session)
        self._log.info('Looking for session with id %d', self.id)
        upd = ergdb.Update(session=self.session, **data)
        self.session.updates.add(upd)
        ergdb.commit()

    def new_stroke(self, data):
        self._log.debug('Stroke number %d', data.get('stroke_count'))
        if self.session:
            self.session.strokes.add(ergdb.Stroke(session=self.session, **data))
            ergdb.commit()

    def new_split(self, data):
        self._log.debug('New split %s', data)
        if self.session:
            self.session.splits.add(ergdb.Split(session=self.session, **data))
            ergdb.commit()

    def new_summary(self, data):
        self._log.debug('New summary %s', data)
        if not self._complete:
            self.session.update(**data)
            ergdb.commit()
            self._complete = True

    @property
    def active(self):
        return self._state == erg.RowingState.ACTIVE

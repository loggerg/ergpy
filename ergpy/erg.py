from pygatt.backends import GATTToolBackend, BLEAddressType
from ergpy.constants import *
from ergpy import ErgException
import datetime
import threading
import logging
import queue

log = logging.getLogger('ergpy')

TIMEOUT = 5
ERROR_HR = 255

NAME = 'ConceptBluePy'
VERSION = __version__ = (0,1)

BASE_UUID = 'ce06{}-43e5-11e4-916c-0800200c9a66'


def _ch(ch): return BASE_UUID.format(ch)


ch31 = _ch('0031')
ch32 = _ch('0032')
ch33 = _ch('0033')
ch35 = _ch('0035')
ch36 = _ch('0036')
ch37 = _ch('0037')
ch38 = _ch('0038')
ch39 = _ch('0039')
ch3a = _ch('003a')


class Erg:
    def __init__(self, mac, dev='hci0'):
        self._log = log.getChild('Erg')
        self._mac = mac
        self.gatt = None
        self._usb = dev
        self.configured = False

        self._general_handler = UpdateHandler('elapsed', 3, name='general_handler')  # salutes
        self._stroke_handler = UpdateHandler('stroke_count', 3, name='stroke_handler')
        self._split_handler = UpdateHandler('number', 2, name='split_handler')
        self._summary_handler = UpdateHandler('log_entry_datetime', 2, name='summary_handler')

    def add_stroke_handler(self, handler):
        """Add callback to receive updates on each stroke"""
        self._stroke_handler.add_listener(handler)

    def remove_stroke_handler(self, handler):
        self._stroke_handler.discard_listener(handler)

    def add_summary_handler(self, handler):
        """Add callback to reveive summary at the end of workout"""
        self._summary_handler.add_listener(handler)

    def remove_summary_handler(self, handler):
        self._summary_handler.discard_listener(handler)

    def add_general_handler(self, handler):
        """Add callback to receive rowing state updates"""
        self._general_handler.add_listener(handler)

    def remove_general_handler(self, handler):
        self._general_handler.discard_listener(handler)

    def add_split_handler(self, handler):
        """Add callback for split/interval summary"""
        self._split_handler.add_listener(handler)

    def remove_split_handler(self, handler):
        self._split_handler.discard_listener(handler)

    def configure(self):
        """Create BT connection and read HW data"""
        self._log.debug('configuring')
        try:
            self.gatt = GATTToolBackend(hci_device=self._usb)
            self.gatt.start()
            self.dev = self.gatt.connect(self._mac, address_type=BLEAddressType.random, timeout=10)
            self.serial = self.dev.char_read(_ch('0012')).decode()
            self.hardware = self.dev.char_read(_ch('0013')).decode()
            self.firmware = self.dev.char_read(_ch('0014')).decode()
            self.manufacturer = self.dev.char_read(_ch('0015')).decode()
            self._log.info('Configured')
            self.configured = True
        except Exception as e:
            self._log.error('Failed to configure', exc_info=e)
            raise ErgException('Could not configure erg', e)

    def start(self):
        """Subscribe to everything"""
        self._log.info('Starting subscriptions')
        if not self.configured:
            raise ValueError('Erg is not configured')
        try:
# -------UPDATE
            self._log.debug('Subscribing to 0x0031 - general')
            self.dev.subscribe(ch31, self._handle31)  # general
            self._log.debug('Subscribing to 0x0032 - general 2')
            self.dev.subscribe(ch32, self._handle32)  # general2
            self._log.debug('Subscribing to 0x0033 - general 3')
            self.dev.subscribe(ch33, self._handle33)  # general3
# -------STROKE
            self._log.debug('Subscribing to 0x0035 - stroke')
            self.dev.subscribe(ch35, self._handle35)  # stroke
            self._log.debug('Subscribing to 0x0036 - stroke 2')
            self.dev.subscribe(ch36, self._handle36)  # stroke2
# -------SPLIT/INTERVAL
            self._log.debug('Subscribing to 0x0037 - split/interval')
            self.dev.subscribe(ch37, self._handle37)  # split
            self._log.debug('Subscribing to 0x0038 - split/interval 2')
            self.dev.subscribe(ch38, self._handle38)  # split2
# -------SUMMARY
            self._log.debug('Subscribing to 0x0039 - summary')
            self.dev.subscribe(ch39, self._handle39)  # workout1
            self._log.debug('Subscribing to 0x003a - summary 2')
            self.dev.subscribe(ch3a, self._handle3a)  # workout2
            self._general_handler.start()
            self._stroke_handler.start()
            self._split_handler.start()
            self._summary_handler.start()
        except Exception as e:
            self._log.error('Failed to subscribe', exc_info=e)
            self.stop()
        else:
            self._log.info('Started')

    def stop(self):
        """Unsubscribe from all updated"""
        self._log.info('Stopping erg')
        for ch in [ch31, ch32, ch33, ch35, ch36, ch37, ch38, ch39, ch3a]:
            try:
                self.dev.unsubscribe(ch)
            except Exception as e:
                self._log.error('Error unsubscribing from channel %s', ch, exc_info=e)
        self._general_handler.stop(True)
        self._stroke_handler.stop(True)
        self._split_handler.stop(True)
        self._summary_handler.stop(True)
        self._log.debug('Erg stopped')

    def disconnect(self):
        """Disconnect BT connection and stop controller"""
        try:
            self._log.info('Disconnecting')
            self.dev.disconnect()
            self.gatt.stop()
        except Exception as e:
            self._log.error('Error disconnecting/stopping bluetooth connection', exc_info=e)

    def __enter__(self):
        self.configure()
        self.start()

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.stop()
        except Exception as e:
            self._log.error('Failed to stop erg')
        try:
            self.disconnect()
        except Exception as e:
            self._log.error('Failed to disconnect erg')

    def _handle31(self, channel, dat):
        """C2 rowing general status"""

        self._log.debug('Handling general update 31 from channel %d', channel)
        _check_dat_length(dat, 19)
        update = {}
        update['elapsed'] = 0.01 * _convert(*dat[:3])
        update['distance'] = 0.1 * _convert(*dat[3:6])
        update['workout_type'] = WorkoutType(_convert(dat[6]))
        update['interval_type'] = IntervalType(_convert(dat[7]))
        update['workout_state'] = WorkoutState(_convert(dat[8]))
        update['rowing_state'] = RowingState(_convert(dat[9]))
        update['stroke_state'] = StrokeState(_convert(dat[10]))
        update['total_work_distance'] = _convert(*dat[11:14])
        update['workout_duration'] = 0.01 * _convert(*dat[14:17])
        update['workout_duration_type'] = WorkoutDurationType(_convert(dat[17]))
        update['drag_factor'] = _convert(dat[18])
        self._general_handler.update(update)

    def _handle32(self, channel, dat):
        """C2 rowing additional status 1"""

        self._log.debug('Handling general update 32 from channel %d', channel)
        _check_dat_length(dat, 16)
        update = {}
        update['elapsed'] = 0.01 * _convert(*dat[:3])
        update['speed'] = 0.001 * _convert(*dat[3:5])
        update['rate'] = min(127, _convert(dat[5]))
        hr = _convert(dat[6])
        update['heartrate'] = hr if hr != ERROR_HR else None
        update['pace'] = 0.01 * _convert(*dat[7:9])
        update['average_pace'] = 0.01 * _convert(*dat[9:11])
        update['rest_distance'] = _convert(*dat[11:13])
        update['rest_time'] = 0.01 * _convert(*dat[13:16])
        self._general_handler.update(update)

    def _handle33(self, channel, dat):
        """C2 rowing additional status 2"""

        self._log.debug('Handling general update 33 from channel %d', channel)
        _check_dat_length(dat, 20)
        update = {}
        update['elapsed'] = 0.01 * _convert(*dat[:3])
        update['interval_count'] = _convert(dat[3])
        update['average_power'] = _convert(*dat[4:6])
        update['total_calories'] = _convert(*dat[6:8])
        update['split_average_pace'] = 0.01 * _convert(*dat[8:10])
        update['split_average_power'] = _convert(*dat[10:12])
        update['split_average_calories'] = _convert(*dat[12:14])
        update['last_split_time'] = 0.1 * _convert(*dat[14:17])
        update['last_split_distance'] = _convert(*dat[17:20])
        self._general_handler.update(update)

    def _handle35(self, channel, dat):
        """C2 rowing stroke data"""

        self._log.debug('Handling stroke update 35 from channel %d', channel)
        _check_dat_length(dat, 20)
        stroke = {}
        stroke['elapsed'] = 0.01 * _convert(*dat[:3])
        stroke['distance'] = 0.1 * _convert(*dat[3:6])
        stroke['drive_length'] = 0.01 * _convert(dat[6])
        stroke['drive_time'] = 0.01 * _convert(dat[7])
        stroke['recovery_time'] = 0.01 * _convert(*dat[8:10])
        stroke['stroke_distance'] = 0.01 * _convert(*dat[10:12])
        stroke['peak_force'] = 0.1 * _convert(*dat[12:14])
        stroke['average_force'] = 0.1 * _convert(*dat[14:16])
        stroke['work_per_stroke'] = 0.1 * _convert(*dat[16:18])
        stroke['stroke_count'] = _convert(*dat[18:20])
        self._stroke_handler.update(stroke)

    def _handle36(self, channel, dat):
        """C2 rowing additional stroke data"""

        self._log.debug('Handling stroke update 36 from channel %d', channel)
        _check_dat_length(dat, 15)
        stroke = {}
        stroke['elapsed'] = 0.01 * _convert(*dat[:3])
        stroke['power'] = _convert(*dat[3:5])
        stroke['calories'] = _convert(*dat[5:7])
        stroke['stroke_count'] = _convert(*dat[7:9])
        stroke['projected_time'] = _convert(*dat[9:12])
        stroke['projected_distance'] = _convert(*dat[12:15])
        self._stroke_handler.update(stroke)

    def _handle37(self, channel, dat):
        """C2 rowing split/interval data"""

        self._log.debug('Handling split update 37 from channel %d', channel)
        _check_dat_length(dat, 18)
        update = {}
        update['elapsed'] = 0.01 * _convert(*dat[:3])
        update['distance'] = 0.1 * _convert(*dat[3:6])
        update['interval_time'] = 0.1 * _convert(*dat[6:9])
        update['interval_distance'] = _convert(*dat[9:12])
        update['rest_time'] = _convert(*dat[12:14])
        update['rest_distance'] = _convert(*dat[14:16])
        update['type'] = IntervalType(_convert(dat[16]))
        update['number'] = _convert(dat[17])
        self._split_handler.update(update)

    def _handle38(self, channel, dat):
        """C2 rowing additional split/interval data"""

        self._log.debug('Handling split update 38 from channel %d', channel)
        _check_dat_length(dat, 18)
        update = {}
        update['elapsed'] = 0.01 * _convert(*dat[:3])
        update['average_rate'] = _convert(dat[3])
        update['work_heartrate'] = _convert(dat[4])
        update['rest_heartrate'] = _convert(dat[5])
        update['average_pace'] = 0.1 * _convert(*dat[6:8])
        update['total_calories'] = _convert(*dat[8:10])
        update['average_calories'] = _convert(*dat[10:12])
        update['speed'] = 0.001 * _convert(*dat[12:14])
        update['power'] = _convert(*dat[14:16])
        update['average_drag_factor'] = _convert(dat[16])
        update['number'] = _convert(dat[17])
        self._split_handler.update(update)

    def _handle39(self, channel, dat):
        """C2 rowing end of workout summary data characteristic"""

        self._log.debug('Handling summary update 39 from channel %d', channel)
        _check_dat_length(dat, 20)
        update = {}
        update['log_entry_datetime'] = _decode_time(*dat[:4])
        update['elapsed'] = 0.01 * _convert(*dat[4:7])
        update['distance'] = 0.1 * _convert(*dat[7:10])
        update['average_rate'] = _convert(dat[10])
        update['ending_heartrate'] = _convert(dat[11])
        update['average_heartrate'] = _convert(dat[12])
        update['min_heartrate'] = _convert(dat[13])
        update['max_heartrate'] = _convert(dat[14])
        update['average_drag_factor'] = _convert(dat[15])
        update['recovery_heartrate'] = _convert(dat[16])
        update['workout_type'] = WorkoutType(_convert(dat[17]))
        update['average_pace'] = 0.1 * _convert(*dat[18:20])
        self._summary_handler.update(update)

    def _handle3a(self, channel, dat):
        """C2 rowing end of workout additional summary data characteristic 1"""

        self._log.debug('Handling summary update 3a from channel %d', channel)
        _check_dat_length(dat, 19)
        update = {}
        update['log_entry_datetime'] = _decode_time(*dat[:4])
        update['interval_type'] = IntervalType(_convert(dat[4]))
        update['interval_size'] = _convert(*dat[5:7])
        update['interval_count'] = _convert(dat[7])
        update['total_calories'] = _convert(*dat[8:10])
        update['average_power'] = _convert(*dat[10:12])
        update['total_rest_distance'] = _convert(*dat[12:15])
        update['interval_rest_time'] = _convert(*dat[15:17])
        update['average_calories'] = _convert(*dat[17:19])
        self._summary_handler.update(update)


class UpdateHandler:
    def __init__(self, key, expected=1, name='handler'):
        self._log = log.getChild('UpdateHandler')
        if expected < 1:
            raise ValueError('Cannot group fewer than one update')
        self._key = key
        self._name = name
        self._q = queue.Queue()
        self._expected = expected
        self._listeners = set()
        self._stop = False
        self._wait_for_empty = False
        self._current = {}
        self._thread = None

    def add_listener(self, listen):
        self._listeners.add(listen)

    def discard_listener(self, listen):
        self._listeners.discard(listen)

    def _broadcast(self, update):
        if not update:
            self._log.debug('Not broadcasting empty update')
            return
        for listener in self._listeners.copy():
            try:
                self._log.debug('before listener update')
                listener(update)
                self._log.debug('after')
            except Exception as e:
                self._log.error('%s - Error handling update (%s)', self._name, update, exc_info=e)

    def update(self, upd):
        if self._key not in upd:
            raise ValueError('Update {} missing expected key ({})'.format(upd, self._key))
        if not self._thread:
            self._log.debug('%s - Not updating stopped handler', self._name)
        else:
            self._log.debug('rcvd: %s', upd)
            self._q.put(upd)

    def _get_updates(self):
        while not self._stop or self._wait_for_empty:
            try:
                yield self._q.get(timeout=TIMEOUT)
            except queue.Empty as _:
                self._log.debug('No updates for %fs', TIMEOUT)
                if self._stop and self._wait_for_empty:
                    break
        self._log.info('Updater stopped while waiting for update')
        raise UpdaterStoppedException()

    def run(self):
        count = 0
        try:
            for upd in self._get_updates():
                if not self._current or upd[self._key] == self._current[self._key]:
                    self._current.update(upd)
                    count += 1
                    if count >= self._expected:
                        self._broadcast(self._current)
                        count = 0
                        self._current = {}
                else:
                    self._broadcast(self._current)
                    self._current = upd
                    count = 1

        except UpdaterStoppedException as use:
            self._log.info('Stopping handler')

        self._log.info('Closing handler')

    def stop(self, wait_for_empty=False):
        self._log.info('Stopping %s', self._name)
        self._stop = True
        self._wait_for_empty=wait_for_empty
        if self._current:
            self._broadcast(self._current)
        self._log.info('Waiting for handler to stop')
        if self._thread:
            self._thread.join(2+TIMEOUT)
            if self._thread.isAlive():
                self._log.warn('UpdateHandler thread left running')
            else:
                self._thread = None
        self._log.debug('%s stopped', self._name)

    def start(self):
        self._log.info('Starting %s', self._name)
        if not self._thread:
            self._thread = threading.Thread(target=self.run, daemon=True, name=self._name)
        self._thread.start()
        self._log.debug('%s started', self._name)

    def __enter__(self):
        # self._log.debug('Entering context')
        self.start()
        # self._log.debug('Leaving __enter__')

    def __exit__(self, exc_type, exc_value, traceback):
        # self._log.debug('Exiting context')
        self.stop(True)

def discover(dev='hci0', timeout=5):
    """Scan for rowing machines"""
    log.debug('Scanning for rowing machines using: %s with timeout %s', dev, timeout)
    try:
        gatt = GATTToolBackend(hci_device=dev)
        gatt.reset()
        log.debug('Reset device')
        devices = gatt.scan(timeout=timeout)
        return devices
    except Exception as e:
        log.error('Could not scan for devices', exc_info=e)
        raise ErgException(('Could not scan for devices: '
                'check BLE adapter is present'))


def _check_dat_length(dat, length):
    if len(dat) != length:
        raise ValueError('Incorrect data length received ({}, {}) (expected {})'.format(
                len(dat),
                dat,
                length))


def _convert(*b, endian='little'):
    return int.from_bytes(b, endian)


def _decode_time(dl, dh, tl, th):
    log.debug('time in: %d:%d:%d:%d', dh, dl, th, tl)
    hour = th
    minute = tl
    date = _convert(dl, dh)
    year = 2000 + (date >> 9)
    month = date % 16
    day = (date >> 4) % 32
    log.debug('time out: %d/%d/%d %d:%d', year, month, day, hour, minute)
    return datetime.datetime(year, month, day, hour, minute)


class ErgUpdateError(ErgException):
    pass

class UpdaterStoppedException(ErgException):
    pass

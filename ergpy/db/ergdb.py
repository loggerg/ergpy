from ergpy import constants
from ergpy import ErgException
from datetime import datetime
from itertools import groupby
from pony.orm import Optional, PrimaryKey, Required, Set, Database, commit, OrmError, db_session, LongStr, desc
import logging

log = logging.getLogger('ergdb')
_wt = constants.WorkoutType
INTERVAL_SESSIONS = {
        _wt.FIXEDDIST_INTERVAL.value,
        _wt.FIXEDTIME_INTERVAL.value,
        _wt.VARIABLE_INTERVAL.value,
        _wt.VARIABLE_UNDEFINEDREST_INTERVAL.value
}

db = Database()


class EnumRegister:
    def __init__(self):
        self.entities = []

    def __call__(self, db_type):
        self.entities.append(db_type)
        return db_type

    @db_session
    def initialise(self):
        for db_type in self.entities:
            for member in getattr(constants, db_type.__name__).__members__.values():
                try:
                    db_type(id=member.value, name=member.name)
                    commit()
                except OrmError as oe:
                    pass
                    # print('caught ', oe)


db_constants = EnumRegister()


class ErgEnum:
    def __str__(self):
        return '{0.__class__.__name__}.{0.name} - {0.id}'.format(self)


class Session(db.Entity):
    id = PrimaryKey(int, auto=True)
    date = Required(datetime)
    comments = Optional(LongStr)
    splits = Set('Split', cascade_delete=True)
    updates = Set('Update')
    strokes = Set('Stroke')
    heartrate_belt = Optional('HeartrateBelt')
    log_entry_datetime = Optional(datetime, unique=True)
    elapsed = Optional(float)
    distance = Optional(float)
    average_rate = Optional(int)
    ending_heartrate = Optional(int)
    average_heartrate = Optional(int)
    min_heartrate = Optional(int)
    max_heartrate = Optional(int)
    recovery_heartrate = Optional(int)
    average_drag_factor = Optional(int)
    workout_type = Optional('WorkoutType')
    average_pace = Optional(float)  # seconds per 500m
    interval_type = Optional('IntervalType')
    interval_size = Optional(int)  # meters or seconds
    interval_count = Optional(int)
    total_calories = Optional(int)
    average_power = Optional(int)
    total_rest_distance = Optional(int)
    interval_rest_time = Optional(float)
    average_calories = Optional(int)  # calories per hour

    def update(self, **kwargs):
        if 'interval_type' in kwargs:
            kwargs['interval_type'] = IntervalType[kwargs['interval_type'].value]
        if 'workout_type' in kwargs:
            kwargs['workout_type'] = WorkoutType[kwargs['workout_type'].value]
        for upd in kwargs:
            setattr(self, upd, kwargs[upd])

    def _split_info(self, num):
        """Get info stats on interval or session"""
        if self.workout_type.id in INTERVAL_SESSIONS:
            _splits = list(self.splits.order_by(lambda s:s.number))
            spl = _splits[num]
            return {'distance': spl.interval_distance,
                    'time': spl.interval_time,
                    'time_offset': num and _splits[num-1].rest_time
                    }
        else:
            return {'distance': self.distance,
                    'time': self.elapsed,
                    'time_offset': 0}

    def _find_stroke_splits(self):
        """
        Generate tuples of (split/interval, stroke)

        Strokes have no record of which split they were part of but stroke count
        resets for each so increment each time count decreases
        """
        number = 0
        split = 0
        for stroke in self.strokes.order_by(lambda s: s.id):
            if number > stroke.stroke_count:
                split += 1
            number = stroke.stroke_count
            yield (self._split_info(split), stroke)

    def split_strokes(self):
        """
        Generate iterables of strokes for each split

        @returns iterable of (key, iterable of strokes)
        """
        for k, group in groupby(self._find_stroke_splits(), lambda s: s[0]):
            yield k, map(lambda s: s[1], group)

    def split_updates(self):
        return groupby(
                self.updates
                    .filter(lambda u: u.interval_count is not None)
                    .filter(lambda u: u.interval_type.id != constants.IntervalType.REST.value)
                    .order_by(lambda u: u.id),
                lambda u: self._split_info(u.interval_count)
        )

    def __init__(self, **kwargs):
        if 'interval_type' in kwargs:
            kwargs['interval_type'] = IntervalType[kwargs['interval_type'].value]
        if 'workout_type' in kwargs:
            kwargs['workout_type'] = WorkoutType[kwargs['workout_type'].value]
        super().__init__(**kwargs)

    def __str__(self):
        return 'Session {0.id}: {0.date}, {0.distance}m, {0.elapsed}s'.format(self)

    def __format__(self, spec):
        return str(self)


class Stroke(db.Entity):
    id = PrimaryKey(int, auto=True)
    session = Required(Session)
    elapsed = Required(float)
    distance = Optional(float)
    drive_length = Optional(float)
    drive_time = Optional(float)
    recovery_time = Optional(float)
    stroke_distance = Optional(float)
    peak_force = Optional(float)
    average_force = Optional(float)
    work_per_stroke = Optional(float)  # Joules
    stroke_count = Required(int)
    power = Optional(int, size=16)
    calories = Optional(int)
    projected_time = Optional(float)
    projected_distance = Optional(int)

    def __str__(self):
        return 'Stroke: {}, {:.2f}s, {:.2f}, {}W'.format(self.stroke_count, self.elapsed or 0, self.distance or 0, self.power)


class Update(db.Entity):
    id = PrimaryKey(int, auto=True)
    elapsed = Required(float)
    distance = Optional(float)
    workout_type = Optional('WorkoutType')
    interval_type = Optional('IntervalType')
    workout_state = Optional('WorkoutState')
    rowing_state = Optional('RowingState')
    stroke_state = Optional('StrokeState')
    total_work_distance = Optional(float)
    workout_duration = Optional(float)
    workout_duration_type = Optional('WorkoutDurationType')
    drag_factor = Optional(int)
    speed = Optional(float)
    rate = Optional(int, size=8)
    heartrate = Optional(int)
    pace = Optional(float)
    average_pace = Optional(float)
    rest_distance = Optional(float)
    rest_time = Optional(float)
    interval_count = Optional(int)
    average_power = Optional(int)
    total_calories = Optional(int)
    split_average_pace = Optional(float)
    split_average_power = Optional(float)
    split_average_calories = Optional(float)
    last_split_time = Optional(float)
    last_split_distance = Optional(float)
    session = Required(Session)

    def __init__(self, **kwargs):
        if 'workout_type' in kwargs:
            kwargs['workout_type'] = WorkoutType[kwargs['workout_type'].value]
        if 'interval_type' in kwargs:
            kwargs['interval_type'] = IntervalType[kwargs['interval_type'].value]
        if 'workout_state' in kwargs:
            kwargs['workout_state'] = WorkoutState[kwargs['workout_state'].value]
        if 'rowing_state' in kwargs:
            kwargs['rowing_state'] = RowingState[kwargs['rowing_state'].value]
        if 'stroke_state' in kwargs:
            kwargs['stroke_state'] = StrokeState[kwargs['stroke_state'].value]
        if 'workout_duration_type' in kwargs:
            kwargs['workout_duration_type'] = WorkoutDurationType[kwargs['workout_duration_type'].value]
        super().__init__(**kwargs)
        log.info('init')

    def __str__(self):
        return 'Update: {0.elapsed:.2f}s, {0.distance:.2f}m, r{0.rate}'.format(self)

@db_constants
class WorkoutType(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)
    summaries = Set('Session')


@db_constants
class IntervalType(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)
    summary = Optional('Session')
    splits = Set('Split')


@db_constants
class WorkoutState(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)


@db_constants
class RowingState(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)


@db_constants
class StrokeState(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)


@db_constants
class WorkoutDurationType(db.Entity, ErgEnum):
    id = PrimaryKey(int)
    name = Required(str, unique=True)
    updates = Set(Update)


class Split(db.Entity):
    id=PrimaryKey(int, auto=True)
    elapsed = Required(float)
    distance = Optional(float)
    interval_time = Optional(float)
    interval_distance = Optional(float)
    rest_time = Optional(float)
    rest_distance = Optional(float)
    type = Optional('IntervalType')
    number = Required(int)
    average_rate = Optional(int)
    work_heartrate = Optional(int)
    rest_heartrate = Optional(int)
    average_pace = Optional(float)  # seconds per 500m
    total_calories = Optional(int)
    average_calories = Optional(int)  # average calories/hr
    speed = Optional(float)
    power = Optional(int)
    average_drag_factor = Optional(int)
    session = Required(Session)

    def __init__(self, **kwargs):
        if 'type' in kwargs:
            kwargs['type'] = IntervalType[kwargs['type'].value]
        super().__init__(**kwargs)

    def __str__(self):
        return 'Split {0.number}: {0.elapsed:.2f}s, {0.distance:.2f}m, {0.average_pace:.1f}s/500m, r{0.average_rate}'.format(self)


class HeartrateBelt(db.Entity):
    id = PrimaryKey(int, auto=True)
    manufacturer_id = Required(int)
    device_type = Required(int)
    belt_id = Required(int)
    sessions = Set(Session)


class AppData(db.Entity):
    """General app data (version/date initialised/etc)"""
    id = PrimaryKey(int, auto=True)
    key = Required(str, unique=True)
    value = Required(str)


def init(db_file):
    try:
        a = db.bind('sqlite', db_file, create_db=True)
    except Exception as e:
        log.error('Could not create or access db file, does path %s exist?', db_file)
        raise ErgException('Could not access db', e)
    db.generate_mapping(create_tables=True)
    db_constants.initialise()


@db_session
def initialise():
    if AppData.exists(lambda ad: ad.key == 'initialised'):
        print('already initialised')
        return
    WorkoutType(id=0, name='JUSTROW_NOSPLITS')
    WorkoutType(id=1, name='JUSTROW_SPLITS')
    WorkoutType(id=2, name='FIXEDDIST_NOSPLITS')
    WorkoutType(id=3, name='FIXEDDIST_SPLITS')
    WorkoutType(id=4, name='FIXEDTIME_NOSPLITS')
    WorkoutType(id=5, name='FIXEDTIME_SPLITS')
    WorkoutType(id=6, name='FIXEDTIME_INTERVAL')
    WorkoutType(id=7, name='FIXEDDIST_INTERVAL')
    WorkoutType(id=8, name='VARIABLE_INTERVAL')
    WorkoutType(id=9, name='VARIABLE_UNDEFINED_INTERVAL')
    WorkoutType(id=10, name='FIXED_CALORIE')
    WorkoutType(id=11, name='FIXED_WATTMINUTES')
    WorkoutType(id=12, name='NUM')
    WorkoutType(id=13, name='THIRTEEN')
    WorkoutType(id=14, name='FOURTEEN')
    WorkoutType(id=15, name='FIFTEEN')
    WorkoutType(id=16, name='SIXTEEN')

    IntervalType(id=0, name='TIME')
    IntervalType(id=1, name='DIST')
    IntervalType(id=2, name='REST')
    IntervalType(id=3, name='TIMERESTUNDEFINED')
    IntervalType(id=4, name='DISTANCERESTUNDEFINED')
    IntervalType(id=5, name='RESTUNDEFINED')
    IntervalType(id=6, name='CAL')
    IntervalType(id=7, name='CALRESTUNDEFINED')
    IntervalType(id=8, name='WATTMINUTE')
    IntervalType(id=9, name='WATTMINUTERESTUNDEFINED')
    IntervalType(id=255, name='INTERVALTYPE_NONE')

    WorkoutState(id=0, name='WAITTOBEGIN')
    WorkoutState(id=1, name='WORKOUTROW')
    WorkoutState(id=2, name='COUNTDOWNPAUSE')
    WorkoutState(id=3, name='INTERVALREST')
    WorkoutState(id=4, name='INTERVALWORKTIME')
    WorkoutState(id=5, name='INTERVALWORKDISTANCE')
    WorkoutState(id=6, name='INTERVALRESTENDTOWORKTIME')
    WorkoutState(id=7, name='INTERVALRESTENDTOWORKDISTANCE')
    WorkoutState(id=8, name='INTERVALWORKTIMETOREST')
    WorkoutState(id=9, name='INTERVALWORKDISTANCETOREST')
    WorkoutState(id=10, name='WORKOUTEND')
    WorkoutState(id=11, name='TERMINATE')
    WorkoutState(id=12, name='WORKOUTLOGGED')
    WorkoutState(id=13, name='REARM')

    RowingState(id=0, name='INACTIVE')
    RowingState(id=1, name='ACTIVE')

    StrokeState(id=0, name='WAITING_FOR_WHEEL_TO_REACH_MIN_SPEED_STATE')
    StrokeState(id=1, name='WAITING_FOR_WHEEL_TO_ACCELERATE_STATE')
    StrokeState(id=2, name='DRIVING_STATE')
    StrokeState(id=3, name='DWELLING_AFTER_DRIVE_STATE')
    StrokeState(id=4, name='STROKESTATE_RECOVERY_STATE')

    WorkoutDurationType(id=0, name='TIME_DURATION')
    WorkoutDurationType(id=0X40, name='CALORIES_DURATION')
    WorkoutDurationType(id=0X80, name='DISTANCE_DURATION')
    WorkoutDurationType(id=0XC0, name='WATTS_DURATION')

    AppData(key='initialised', value='True')
    print('initialised')


if __name__ == "__main__":
    initialise()

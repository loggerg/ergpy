import toml
import os
import appdirs
from ergpy import NAME, VERSION

DEFAULT = {'output_dir': '~/loggerg',
        'format': 'tcx',
        'file_template': 'erg_{date:%Y%m%d}_{date:%H%M}.tcx',
        'include_rest': False,
        'database': os.path.join(appdirs.user_data_dir(NAME), 'loggerg.db'),
        'ergs': {},
        'strava_timeout': 20,
        'private': False
        }

class Configuration:
    def __init__(self):
        self._rc_file = None
        self._conf = DEFAULT.copy()

    def init(self, rc_file=None):
        self._conf = DEFAULT.copy()
        if rc_file:
            rc_file = os.path.expanduser(rc_file)
            rc_file = os.path.expandvars(rc_file)
            self._rc_file = rc_file
            try:
                self._conf.update(toml.load(rc_file))
            except FileNotFoundError as fnfe:
                raise ValueError('File does not exist')

    def __getattr__(self, name):
        if name[0] == '_':
            raise AttributeError('Not found')
        try:
            return self._conf[name]
        except KeyError as ke:
            raise AttributeError('Key not present in config')

    def __setattr__(self, name, value):
        if name[0] == '_':
            super().__setattr__(name, value)
            return
        self._conf[name] = value

    def __delattr__(self, name):
        if name[0] == '_':
            super().__delattr__(name)
            return
        self._conf.pop(name)

    def __contains__(self, name):
        if name[0] == '_':
            return False
        return name in self._conf

    def store(self):
        if self._rc_file is not None:
            with open(self._rc_file, 'w') as rc:
                toml.dump(self._conf, rc)

from enum import Enum, unique

@unique
class WorkoutType(Enum):
    JUSTROW_NOSPLITS = 0
    JUSTROW_SPLITS = 1
    FIXEDDIST_NOSPLITS = 2
    FIXEDDIST_SPLITS = 3
    FIXEDTIME_NOSPLITS = 4
    FIXEDTIME_SPLITS = 5
    FIXEDTIME_INTERVAL = 6
    FIXEDDIST_INTERVAL = 7
    VARIABLE_INTERVAL = 8
    VARIABLE_UNDEFINEDREST_INTERVAL = 9
    FIXED_CALORIE = 10
    FIXED_WATTMINUTES = 11
    NUM = 12
    #Not sure what these ones are but they appear occasionally
    #and crash if they're not defined
    #TODO: figure out a reason
    THIRTEEN = 13
    FOURTEEN = 14
    FIFTEEN = 15
    SIXTEEN = 16

@unique
class IntervalType(Enum):
    TIME = 0
    DIST = 1
    REST = 2
    TIMERESTUNDEFINED = 3
    DISTANCERESTUNDEFINED = 4
    RESTUNDEFINED = 5
    CAL = 6
    CALRESTUNDEFINED = 7
    WATTMINUTE = 8
    WATTMINUTERESTUNDEFINED = 9
    INTERVALTYPE_NONE = 255

@unique
class WorkoutState(Enum):
    WAITTOBEGIN = 0
    WORKOUTROW = 1
    COUNTDOWNPAUSE = 2
    INTERVALREST = 3
    INTERVALWORKTIME = 4
    INTERVALWORKDISTANCE = 5
    INTERVALRESTENDTOWORKTIME = 6
    INTERVALRESTENDTOWORKDISTANCE = 7
    INTERVALWORKTIMETOREST = 8
    INTERVALWORKDISTANCETOREST = 9
    WORKOUTEND = 10
    TERMINATE = 11
    WORKOUTLOGGED = 12
    REARM = 13

@unique
class RowingState(Enum):
    INACTIVE = 0
    ACTIVE = 1

@unique
class StrokeState(Enum):
    WAITING_FOR_WHEEL_TO_REACH_MIN_SPEED_STATE = 0
    WAITING_FOR_WHEEL_TO_ACCELERATE_STATE = 1
    DRIVING_STATE = 2
    DWELLING_AFTER_DRIVE_STATE = 3
    STROKESTATE_RECOVERY_STATE = 4

@unique
class WorkoutDurationType(Enum):
    TIME_DURATION = 0
    CALORIES_DURATION = 0X40
    DISTANCE_DURATION = 0X80
    WATTS_DURATION = 0XC0


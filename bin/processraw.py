from ergpy import erg
handles = {}
handles[29] = erg.Erg._handle31
handles[32] = erg.Erg._handle32
handles[35] = erg.Erg._handle33
handles[40] = erg.Erg._handle35
handles[43] = erg.Erg._handle36
handles[46] = erg.Erg._handle37
handles[49] = erg.Erg._handle38
handles[52] = erg.Erg._handle39
handles[55] = erg.Erg._handle3a
class X:
    def __init__(self, d):
        self.other = d
    def __call__(self, *a, **kw):
        self.other.update(kw)
        return 1;
class T():
    def __repr__(self):
        return '\t'.join('{}: {}'.format(k,v) for k,v in self.__dict__.items())
    def __getattr__(self, attr):
        return X(self.__dict__)
gen = []
stroke = []
summary = []
split = []
lists = {29:gen, 32:gen, 35:gen, 40:stroke, 43:stroke, 46:split, 49:split, 52:summary, 55:summary}
with open('ergpy/tests/intervals','r') as dat:
    for line in dat:
        ds = line.split(', ')
        time = float(ds[0])
        ch = int(ds[1])
        data = eval(ds[2])
        t = T()
        handles[ch](t, ch, data)
        lists[ch].append((time, ch, t))
with open('/tmp/ergintsplit', 'w') as out:
    for update in gen:
        out.write('{:.3f} - {} - {}'.format(update[0], update[1], update[2]))
        out.write('\n')
    for update in stroke:
        out.write('{:.3f} - {} - {}'.format(update[0], update[1], update[2]))
        out.write('\n')
